class CommentsController < ApplicationController
  before_action :set_event
  before_action :set_comment, only: :destroy

  def create
    @comment = @event.comments.new(comment_params)
    @comment.user = current_user
    @comment.save
  end

  def destroy
    @comment.destroy
  end

  private

  def set_event
    @event = Event.find(params[:event_id])
  end

  def set_comment
    @comment = @event.comments.find(params[:id])
  end

  def comment_params
    params.require(:comment).permit(:content)

  end
end